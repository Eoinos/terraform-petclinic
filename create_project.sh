#!/bin/bash

PROJECT_NAME="itt-petclinic"
TERRAFORM_SERVICE_ACCOUNT_NAME="terraform"
BILLING_ACCOUNT="xxxxxxxxxxx"

gcloud projects create $PROJECT_NAME
gcloud config set project $PROJECT_NAME
gcloud iam service-accounts create $TERRAFORM_SERVICE_ACCOUNT_NAME
gcloud projects add-iam-policy-binding $PROJECT_NAME --member "serviceAccount:$TERRAFORM_SERVICE_ACCOUNT_NAME@$PROJECT_NAME.iam.gserviceaccount.com" --role "roles/owner"
gcloud iam service-accounts keys create terraform-sa.json --iam-account $TERRAFORM_SERVICE_ACCOUNT_NAME@$PROJECT_NAME.iam.gserviceaccount.com
gcloud beta billing projects link $PROJECT_NAME --billing-account="$BILLING_ACCOUNT"
