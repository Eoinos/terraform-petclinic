provider "google" {
  credentials = file("../terraform-sa.json")
  project     = var.project_id
  region      = var.region
}

provider "gitlab" {
    token = var.gitlab_token
}

# APIs
resource "google_project_service" "crm" {
  service = "cloudresourcemanager.googleapis.com"
  disable_dependent_services = true
}

resource "google_project_service" "iam" {
  service = "iam.googleapis.com"
  disable_dependent_services = true
  depends_on = [google_project_service.crm]
}

resource "google_project_service" "container_registry" {
  service = "containerregistry.googleapis.com"
  disable_dependent_services = true
  depends_on = [google_project_service.crm]
}

resource "google_project_service" "cloud_run" {
  service = "run.googleapis.com"
  disable_dependent_services = true
  depends_on = [google_project_service.crm, google_project_service.container_registry]
}


### GitLab

# Service Account
resource "google_service_account" "gitlab" {
  account_id   = "gitlab"
  description  = "Allow GitLab to push images to GCR"
  depends_on   = [google_project_service.iam]
}

resource "google_project_iam_member" "gitlab_sa" {
  role    = "roles/iam.serviceAccountUser"
  member  = "serviceAccount:${google_service_account.gitlab.email}"
}

resource "google_project_iam_member" "gitlab_gcr" {
  role    = "roles/storage.admin"
  member  = "serviceAccount:${google_service_account.gitlab.email}"
}

resource "google_project_iam_member" "gitlab_cloud_run" {
  role    = "roles/run.admin"
  member  = "serviceAccount:${google_service_account.gitlab.email}"
}

resource "google_service_account_key" "gitlab" {
  service_account_id = google_service_account.gitlab.name
  public_key_type    = "TYPE_X509_PEM_FILE"
}

# Add relevant vars to GitLab project
resource "gitlab_project_variable" "gcr_credentials" {
   project           = var.gitlab_project_id
   key               = "GCP_SERVICE_KEY"
   variable_type     = "file"
   value             = base64decode(google_service_account_key.gitlab.private_key)
   masked            = false  # Gives an error
   environment_scope = "*"

}

resource "gitlab_project_variable" "gcp_project_id" {
   project           = var.gitlab_project_id
   key               = "GCP_PROJECT_ID"
   value             = var.project_id
   masked            = true
   environment_scope = "*"
}
